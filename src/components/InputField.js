import React, { Component } from 'react';
import {Field, Control,Button, Input,Container} from 'reactbulma';
import './App.css';


class InputField extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data:[]
    }
  }

  render() {

    return (
      <div style={{ textAlign: 'center'}}>
      <Field hasAddons style={{ justifyContent: 'center'}}>    
        <Control>
          <Input placeholder="กรอกจำนวนรูปที่ต้องการ" onChange={this.props.handleChange} />
        </Control>
        <Control>
          <Button info onClick={this.props.get.bind(this)}>
            ตกลง
          </Button>
        </Control>
      </Field>
      </div>
    );
  }
}

export default InputField;
